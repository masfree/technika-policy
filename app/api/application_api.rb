class ApplicationApi < Grape::API
  format :json

  mount Api::V1
end
