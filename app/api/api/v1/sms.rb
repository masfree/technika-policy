class Api::V1::Sms < Api::V1::Base
  namespace :sms do
    post ':sms_code_id/confirm' do
      ::Sms::Confirm.response(params)
    rescue StandardError => e
      error!(e.message, 403)
    end

    post ':sms_code_id/resend' do
      sms_code = SmsCode.find(params[:sms_code_id])
      return error!('Смс уже подтверждено', 403) if sms_code.confirmed

      sms_code.resend

      present :sms_code, sms_code, with: Api::V1::Entities::SmsCodeEntity
    end
  end
end
