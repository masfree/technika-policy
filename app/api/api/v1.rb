class Api::V1 < Grape::API
  include AuthenticateRequest

  before do
    @log_start_t = Time.now
    Rails.logger.info "  Parameters: #{params.to_hash.except('route_info')}"
  end

  helpers do
    def permitted_params
      @permitted_params ||= ActionController::Parameters.new(declared(params, include_missing: false)).permit!
    end

    def collection
      @policy.result
    end
  end

  after do
    @log_end_t = Time.now
    total_runtime = ((@log_end_t - @log_start_t) * 1000).round(1)
    db_runtime = (ActiveRecord::RuntimeRegistry.sql_runtime || 0).round(1)
    Rails.logger.info "Completed in #{total_runtime}ms (ActiveRecord: #{db_runtime}ms)"
  end

  version 'v1', using: :param, parameter: 'v'

  before do
    authenticate_soft!
    driver_authenticate_soft!
    @policy = Policy.new(user: current_user, driver: current_driver, params: params, route_object: route)
    @policy.validate
  end

  format :json
  content_type :json, 'application/json; charset=utf-8'

  rescue_from ActiveRecord::RecordNotFound do
    error!('Запись не нейдена', 404)
  end

  rescue_from ApplicationController::NotAuthorized do
    error!('Не авторизован', 401)
  end

  rescue_from ApplicationController::NoAccess do
    error!('Нет доступа', 403)
  end

  namespace :v1 do
    mount Api::V1::Tracker
    mount Api::V1::Cabinet
    mount Api::V1::Kinds
    mount Api::V1::Advantages
    mount Api::V1::Instances
    mount Api::V1::Orders
    mount Api::V1::Properties
    mount Api::V1::SignedDocuments
    mount Api::V1::Geocodes
    mount Api::V1::Auth
    mount Api::V1::Faqs
    mount Api::V1::Models
    mount Api::V1::Pages
    mount Api::V1::Users
    mount Api::V1::ContactMessages
    mount Api::V1::OrderMessages
    mount Api::V1::Drivers
    mount Api::V1::Sms
    mount Api::V1::Owners
    mount Api::V1::Materials
    add_swagger_documentation
  end
end
