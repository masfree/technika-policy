class Policy
  attr_reader :user, :driver, :params, :route_object, :factory

  def initialize(user:, driver:, params:, route_object:)
    @user = user
    @driver = driver
    @params = params
    @route_object = route_object
    @factory = PolicyWrapper.new(user: user, driver: driver, params: params, route_object: route_object)
  end

  def validate
    factory.validate
  end

  def result
    factory.result
  end

  def method_missing(method_name, *args, &blk)
    return factory.send(method_name, *args, &blk)
  end
end
