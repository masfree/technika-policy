class Policy::V1::Cabinet::Orders::My < Policy::EndpointBase
  class Base < Policy::Base
    def result
      return Order.my_park if admin?

      current_company.client_orders.my_park
    end

    def validate
      raise ApplicationController::NoAccess unless in_roles?(%w[owner constructor admin])
    end
  end

  class Get < Policy::V1::Cabinet::Orders::My::Base
  end

  class Post < Policy::V1::Cabinet::Orders::My::Base
  end

  class Put < Policy::V1::Cabinet::Orders::My::Base
  end

  class Delete < Policy::V1::Cabinet::Orders::My::Base
  end
end
