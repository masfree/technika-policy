class Policy::V1::Cabinet::Orders::Documents < Policy::EndpointBase
  class Base < Policy::Base
    def result
      return Order.all if admin?

      Order.where(client_id: current_company.id).or(
        Order.where(owner_id: current_company.id)
      ).distinct
    end

    def validate
      true
    end
  end

  class Get < Policy::V1::Cabinet::Orders::Documents::Base
  end

  class Post < Policy::V1::Cabinet::Orders::Documents::Base
  end

  class Put < Policy::V1::Cabinet::Orders::Documents::Base
  end

  class Delete < Policy::V1::Cabinet::Orders::Documents::Base
  end
end
