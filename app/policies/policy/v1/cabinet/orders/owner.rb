class Policy::V1::Cabinet::Orders::Owner < Policy::EndpointBase
  class Base < Policy::Base
    def result
      return Order.not_my_park if admin?

      return current_company.orders_variants.where.not(client_id: current_company.id).where(state: 'tender') if route.include?('tenders')
      return current_company.orders_variants.where.not(client_id: current_company.id) if owner_search?
      current_company.owner_orders.not_my_park
    end

    def validate
      raise ApplicationController::NoAccess unless in_roles?(%w[owner constructor admin])
    end

    def owner_search?
      return true if route.include?('orders/owner/tenders')
      params[:state] == 'tenders' || params[:state] == 'tender' || params[:state] == 'new'
    end
  end

  class Get < Policy::V1::Cabinet::Orders::Owner::Base
  end

  class Post < Policy::V1::Cabinet::Orders::Owner::Base
    def validate
      return true if %w[set_driver set_instance].select { |endpoint| route.include?(endpoint) }.size > 0
      super
    end

    def owner_search?
      return true if %w[offer accept reject].select { |endpoint| route.include?(endpoint) }.size > 0

      false
    end
  end

  class Put < Policy::V1::Cabinet::Orders::Owner::Base
  end

  class Delete < Policy::V1::Cabinet::Orders::Owner::Base
  end
end
