class Policy::V1::Cabinet::Orders::Client < Policy::EndpointBase
  class Base < Policy::Base
    def result
      case
      when admin?
        Order.not_my_park
      else # owner? client?
        current_company.client_orders.not_my_park
      end
    end

    def validate
      raise ApplicationController::NoAccess unless in_roles?(%w[client constructor admin])
      true
    end
  end

  class Get < Policy::V1::Cabinet::Orders::Client::Base
  end

  class Post < Policy::V1::Cabinet::Orders::Client::Base
  end

  class Put < Policy::V1::Cabinet::Orders::Client::Base
  end

  class Delete < Policy::V1::Cabinet::Orders::Client::Base
  end
end
