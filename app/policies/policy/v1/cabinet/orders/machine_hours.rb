class Policy::V1::Cabinet::Orders::MachineHours < Policy::EndpointBase
  class Base < Policy::Base
    def result
      return Order.all if admin?

      return Order.where(client_id: current_company.id) if client?

      Order.where(client_id: current_company.id).or(Order.where(owner_id: current_company.id))
    end
  end

  class Get < Policy::V1::Cabinet::Orders::MachineHours::Base
  end
end
