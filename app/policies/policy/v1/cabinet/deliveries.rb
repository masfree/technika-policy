class Policy::V1::Cabinet::Deliveries < Policy::EndpointBase
  class Base < Policy::Base
    def result
      return owner_offer_result if route.include?('owner')
      client_result
    end

    def owner_result
      return Delivery.where.not(owner_id: nil) if admin?

      current_company.owner_deliveries
    end

    def client_result
      return Delivery.all if admin?

      current_company.deliveries
    end

    def owner_offer_result
      Delivery.where(owner_id: nil)
    end
  end

  class Get < Policy::V1::Cabinet::Deliveries::Base
  end

  class Post < Policy::V1::Cabinet::Deliveries::Base
  end

  class Put < Policy::V1::Cabinet::Deliveries::Base
  end

  class Delete < Policy::V1::Cabinet::Deliveries::Base
  end
end
