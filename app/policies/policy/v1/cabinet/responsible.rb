# Get is get owners companies
# - other occupations control
class Policy::V1::Cabinet::Responsible < Policy::EndpointBase
  class Base < Policy::Base
    def validate
      true
    end
  end

  class Get < Policy::V1::Cabinet::Responsible::Base
  end

  class Post < Policy::V1::Cabinet::Responsible::Base
  end

  class Put < Policy::V1::Cabinet::Responsible::Base
  end

  class Delete < Policy::V1::Cabinet::Responsible::Base
  end
end
