
class Policy::V1::Cabinet::Orders < Policy::EndpointBase
  def policy
    policy_class = case
                   when route.include?('documents')
                     Policy::V1::Cabinet::Orders::Documents
                   when route.include?('my')
                     Policy::V1::Cabinet::Orders::My
                   when route.include?('owner')
                     Policy::V1::Cabinet::Orders::Owner
                   when route.include?('client')
                     Policy::V1::Cabinet::Orders::Client
                   when route.include?('dashboard')
                     Policy::V1::Cabinet::Dashboard
                   else
                     return super
                   end
    return policy_class.new(user: user, driver: driver, route_object: route_object, params: params).policy
  end


  class Base < Policy::Base
    def result
      case
      when admin?
        Order.all
      when owner?
        Order.where(id: current_company.orders_variants.where.not(client_id: current_company.id).ids).or(
          Order.where(owner_id: current_company.id)
        )
      when constructor?
        Order.where(id: current_company.orders_variants.where.not(client_id: current_company.id).ids).or(
          Order.where(client_id: current_company.id)
        ).or(
          Order.where(owner_id: current_company.id)
        ).distinct
      when client?
        current_company.client_orders
      else
        current_company.client_orders
      end
    end

    def validate
      raise ApplicationController::NoAccess unless client? || constructor? || admin? #in_roles?(%w[client constructor admin])
      true
    end
  end

  class Get < Policy::V1::Cabinet::Orders::Base
    def validate
      case
      when route.include?('/all')
      when route.include?(':order_id')
        true
      when route.include?(':id')
        true
      else
        super
      end
    end
  end

  class Post < Policy::V1::Cabinet::Orders::Base
    def validate
      case
      when route.include?('orders/:order_id/work_comments')
        true
      when route.include?('orders/:order_id/machine_hours')
        true
      else
        super
      end
    end
  end

  class Put < Policy::V1::Cabinet::Orders::Base
    def validate
      true
    end

    def result
      return Order.all if admin?
      client_orders = current_company.client_orders
      return client_orders if client_orders

      current_company.owner_orders
    end
  end

  class Delete < Policy::V1::Cabinet::Orders::Base
  end
end

