class Policy::V1::Cabinet::Base < Policy::EndpointBase
  class Base < Policy::Base
    def result
      case
      when admin?
        Order.all
      when owner?
        current_company.owner_orders
      when client?
        current_company.client_orders
      else
        current_company.client_orders
      end
    end
  end

  class Get < Policy::V1::Cabinet::Base::Base
  end

  class Post < Policy::V1::Cabinet::Base::Base
  end

  class Put < Policy::V1::Cabinet::Base::Base
  end

  class Delete < Policy::V1::Cabinet::Base::Base
  end
end
