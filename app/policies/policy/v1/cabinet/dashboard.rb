class Policy::V1::Cabinet::Dashboard < Policy::EndpointBase
  class Base < Policy::Base
    def available_orders
      case
      when admin?
        Order.all
      when owner?
        current_company.owner_orders
      when client?
        current_company.client_orders
      else
        current_company.client_orders
      end
    end

    def available_geozones
      case
      when admin?
        Geozone.all
      when owner?
        Geozone.joins(:orders, :trackers).where("orders.owner_id = ? OR trackers.owner_id = ?",  current_company.id, current_company.id).distinct
      when client?
        Geozone.joins(:orders, :trackers).where("orders.client_id = ? OR trackers.client_id = ?",  current_company.id, current_company.id).distinct
      else
        Geozone.joins(:orders, :trackers).where("orders.client_id = ? OR trackers.client_id = ?",  current_company.id, current_company.id).distinct
      end
    end

    def available_trackers
      case
      when admin?
        Tracker.all
      when owner?
        current_company.owner_trackers
      when client?
        current_company.trackers
      else
        current_company.trackers
      end
    end
  end

  class Get < Policy::V1::Cabinet::Dashboard::Base
  end
end
