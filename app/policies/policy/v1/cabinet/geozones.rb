
class Policy::V1::Cabinet::Geozones < Policy::EndpointBase
  class Base < Policy::Base
    def result
      case
      when admin?
        Geozone.all
      when owner?
        Geozone.joins(:orders, :trackers).where("orders.owner_id = ? OR trackers.owner_id = ?",  current_company.id, current_company.id).distinct
      when client?
        Geozone.joins(:orders, :trackers).where("orders.client_id = ? OR trackers.client_id = ?",  current_company.id, current_company.id).distinct
      when constructor?
        current_company.geozones
      else
        Geozone.joins(:orders, :trackers).where("orders.client_id = ? OR trackers.client_id = ?",  current_company.id, current_company.id).distinct
      end
    end

    def validate
      raise ApplicationController::NotAuthorized unless constructor? || admin?
    end
  end

  class Get < Policy::V1::Cabinet::Geozones::Base
  end

  class Post < Policy::V1::Cabinet::Geozones::Base
  end

  class Put < Policy::V1::Cabinet::Geozones::Base
  end

  class Delete < Policy::V1::Cabinet::Geozones::Base
  end
end
