class Policy::V1::Cabinet::Instances < Policy::EndpointBase
  class Base < Policy::Base
    def result
      case
      when admin?
        Instance.all
      when client?
        Instance.joins(:orders, :trackers).where('orders.id IN(?) OR trackers.client_id = ?', current_company.client_orders.by_state('active').ids, current_company.id).distinct
      else
        current_company.instances
      end
    end

    def validate
      raise ApplicationController::NoAccess if client?
      true
    end
  end

  class Get < Policy::V1::Cabinet::Instances::Base
    def validate
      return super if route.include?('instances/:id')
      true
    end
  end

  class Post < Policy::V1::Cabinet::Instances::Base
  end

  class Put < Policy::V1::Cabinet::Instances::Base
  end

  class Delete < Policy::V1::Cabinet::Instances::Base
  end
end
