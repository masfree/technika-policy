class Policy::V1::Cabinet::Messages < Policy::EndpointBase
  class Base < Policy::Base
    def result
      case
      when admin?
        Order.all
      when owner?
        current_company.owner_orders
      when client?
        current_company.client_orders
      else
        current_company.client_orders
      end
    end
  end

  class Get < Policy::V1::Cabinet::Messages::Base
  end

  class Post < Policy::V1::Cabinet::Messages::Base
    def result
      Order.all
    end
  end

  class Put < Policy::V1::Cabinet::Messages::Base
  end

  class Delete < Policy::V1::Cabinet::Messages::Base
  end
end
