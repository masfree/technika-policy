class Policy::V1::Cabinet::Drivers < Policy::EndpointBase
  class Base < Policy::Base
    def result
      case
      when admin?
        Driver.all
      else
        current_company.drivers
      end
    end

    def validate
      raise ApplicationController::NoAccess if client?
      true
    end
  end

  class Get < Policy::V1::Cabinet::Instances::Base
  end

  class Post < Policy::V1::Cabinet::Instances::Base
  end

  class Put < Policy::V1::Cabinet::Instances::Base
  end

  class Delete < Policy::V1::Cabinet::Instances::Base
  end
end
