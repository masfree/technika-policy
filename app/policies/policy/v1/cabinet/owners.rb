# Get is get owners companies
# - other occupations control
class Policy::V1::Cabinet::Owners < Policy::EndpointBase
  class Base < Policy::Base
    def validate
      return true if admin?
      raise ApplicationController::NoAccess if client?
      super
    end
  end

  class Get < Policy::V1::Cabinet::Owners::Base
    def validate
      true
    end
  end

  class Post < Policy::V1::Cabinet::Owners::Base
  end

  class Put < Policy::V1::Cabinet::Owners::Base
  end

  class Delete < Policy::V1::Cabinet::Owners::Base
  end
end
