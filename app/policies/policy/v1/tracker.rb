class Policy::V1::Tracker < Policy::Base
  def policy
    validate
    policy_class = case
                   when route.include?('owner/trackers')
                     Policy::V1::Tracker::Owner
                   when route.include?('trackers')
                     Policy::V1::Tracker::Trackers
                   else
                     Policy::V1::Tracker::Base::Base
                   end
    policy_class.new(user: user, driver: driver, route_object: route_object, params: params).policy
  end

  def validate
    raise ApplicationController::NotAuthorized if current_user.nil?
  end
end
