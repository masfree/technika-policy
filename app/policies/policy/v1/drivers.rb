class Policy::V1::Drivers < Policy::EndpointBase
  class Base < Policy::Base
    def result
      driver.company.owner_orders
    rescue
      []
    end

    def validate
      raise ApplicationController::NoAccess unless driver?
      true
    end
  end

  class Get < Policy::V1::Drivers::Base
  end

  class Post < Policy::V1::Drivers::Base
    def validate
      return true if route.include?('request_code')
      super
    end
  end

  class Put < Policy::V1::Drivers::Base
  end

  class Delete < Policy::V1::Drivers::Base
  end
end
