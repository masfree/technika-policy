
class Policy::V1::Tracker::Base < Policy::EndpointBase
  class Base < Policy::Base
    def result
      case
      when admin?
        Tracker.all
      when owner?
        current_company.owner_trackers
      when client?
        current_company.trackers
      else
        current_company.trackers
      end
    end
  end

  class Get < Policy::V1::Tracker::Base::Base
  end

  class Post < Policy::V1::Tracker::Base::Base
  end

  class Put < Policy::V1::Tracker::Base::Base
  end

  class Delete < Policy::V1::Tracker::Base::Base
  end
end
