
class Policy::V1::Tracker::Owner < Policy::EndpointBase
  class Base < Policy::Base
    def result
      case
      when admin?
        Tracker.where(owner_id: nil)
      else
        Tracker.search_trackers_for_company(current_company.id)
      end
    end

    def validate
      raise ApplicationController::NoAccess if in_roles?(%w[client])
    end
  end

  class Get < Policy::V1::Tracker::Owner::Base
  end

  class Post < Policy::V1::Tracker::Owner::Base
  end

  class Put < Policy::V1::Tracker::Owner::Base
  end

  class Delete < Policy::V1::Tracker::Owner::Base
  end
end
