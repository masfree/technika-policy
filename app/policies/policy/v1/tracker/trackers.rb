
class Policy::V1::Tracker::Trackers < Policy::EndpointBase
  class Base < Policy::Base
    def result
      case
      when admin?
        Tracker.all
      when owner?
        tracker_result
      when car_owner?
        current_company.trackers
      when client?
        current_company.trackers
      else
        current_company.trackers
      end
    end

    def tracker_result
      case
      when admin?
        Tracker.all
      when car_owner?

        instances_ids = current_company.instances.joins(:tracker_instances).distinct.ids
        # users_ids = current_company.users.ids
        tender_ids = ::Tracker.search_trackers_for_company(current_company.id).where(state: 'waiting').distinct.ids
        tracker_by_points_ids = Tracker.joins(:points, :tracker_instances).where(points: { company_id: current_company.id }).distinct.ids
        tracker_by_instances_ids = Tracker.joins(:tracker_instances).where(tracker_instances: { instance_id: instances_ids }).distinct.ids
        current_company.owner_trackers.or(current_company.trackers).or(Tracker.where(id: tracker_by_points_ids | tracker_by_instances_ids | tender_ids))
      when client?
        tracker_by_points_ids = Tracker.joins(:points, :tracker_instances).where(points: { company_id: current_company.id }).distinct.ids
        current_company.trackers.or(Tracker.where(id: tracker_by_points_ids))
      else
        current_company.trackers
      end
    end

    def validate
      raise ApplicationController::NoAccess if in_roles?(%w[client])
      true
    end
  end

  class Get < Policy::V1::Tracker::Trackers::Base
    def result
      if route.include?('trackers/:id') || route.include?('documents')
        begin
          tr = tracker_result
          tr.find(params[:id])
          return tracker_result
        rescue
          return current_company.responsible_trackers
        end
      end
      super
    end

    def validate
      case
      when route.include?('trackers/my')
        raise ApplicationController::NoAccess if in_roles?(%w[client])
      when route.include?('trackers/:id')
        true
      else
        super
      end
      true
    end
  end

  class Post < Policy::V1::Tracker::Trackers::Base
    def result
      case
      when admin?
        Tracker.all
      when car_owner?
        Tracker.where(client_id: current_company.id).or(Tracker.where(owner_id: current_company.id))
      else
        current_company.trackers
      end
    end

    def validate
      return true if route.include?('documents')
      return true if route.include?('trackers/:id/trips')
      raise ApplicationController::NoAccess if client?
      return true if route.include?('trackers/:id/create_trips')
      raise ApplicationController::NoAccess if in_roles?(%w[client admin])
      if params[:tracker_instances_attributes].present? && in_roles?(%w[client owner admin])
        raise ApplicationController::NoAccess
      end
    end
  end

  class Put < Policy::V1::Tracker::Trackers::Base
    def result
      case
      when admin?
        Tracker.all
      when car_owner?
        Tracker.where(client_id: current_company.id).or(Tracker.where(owner_id: current_company.id))
      else
        current_company.trackers
      end
    end

    def validate
      raise ApplicationController::NoAccess if in_roles?(%w[client])
    end
  end

  class Delete < Policy::V1::Tracker::Trackers::Base
  end
end
