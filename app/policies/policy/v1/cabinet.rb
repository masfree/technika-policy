class Policy::V1::Cabinet < Policy::Base
  def policy
    validate
    policy_class = case
                   when route.include?('responsible')
                     Policy::V1::Cabinet::Responsible
                   when route.include?('deliveries')
                     Policy::V1::Cabinet::Deliveries
                   when route.include?('drivers')
                     Policy::V1::Cabinet::Drivers
                   when route.include?('dashboard')
                     Policy::V1::Cabinet::Dashboard
                   when route.include?('orders')
                     Policy::V1::Cabinet::Orders
                   when route.include?('geozones')
                     Policy::V1::Cabinet::Geozones # TODO WRITE RULES
                   when route.include?('instances')
                     Policy::V1::Cabinet::Instances
                   when route.include?('owners')
                     Policy::V1::Cabinet::Owners
                   when route.include?('messages')
                     Policy::V1::Cabinet::Messages
                   else
                     Policy::V1::Cabinet::Base
                   end
    policy_class.new(user: user, driver: driver, route_object: route_object, params: params).policy
  end

  def validate
    raise ApplicationController::NotAuthorized if current_user.nil?
  end
end
