class Policy::V1::Orders < Policy::EndpointBase
  class Base < Policy::Base
    def validate
      raise ApplicationController::NoAccess unless params[:geozone_id].present? && constructor?
      raise ApplicationController::NoAccess if client?
      true
    end

    def result
      case
      when admin?
        Orders.all
      when owner?
        current_company.owner_orders
      when client?
        current_company.client_orders
      else
        current_company.client_orders
      end
    end
  end

  class Get < Policy::V1::Orders::Base
  end

  class Post < Policy::V1::Orders::Base
    def validate
      return true unless current_user
      raise ApplicationController::NoAccess unless in_roles?(%w[client constructor admin])
      true
    end
  end

  class Put < Policy::V1::Orders::Base
  end

  class Delete < Policy::V1::Orders::Base
  end
end
