class Policy::V1::Instances < Policy::EndpointBase
  class Base < Policy::Base
    def validate
      raise ApplicationController::NoAccess if client?
      super
    end

    def result
      case
      when admin?
        Instance.all
      when owner?
        current_company.instances
      when client?
        []
      else
        []
      end
    end
  end

  class Get < Policy::V1::Instances::Base
  end

  class Post < Policy::V1::Instances::Base
  end

  class Put < Policy::V1::Instances::Base
  end

  class Delete < Policy::V1::Instances::Base
  end
end
