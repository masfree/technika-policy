class Policy::Base
  attr_reader :user, :driver, :route_object, :params, :route, :request_method, :current_user, :current_company
  def initialize(user:, driver:, route_object:, params:)
    @user = user
    @driver = driver
    @current_user = user
    @current_company = user&.company
    @route_object = route_object
    @route = route_object.path.gsub('(.:format)', '')
    @request_method = route_object.request_method.capitalize
    @params = params
  end

  def policy
    @policy ||= self
  end

  def validate
    policy.self_validate
  end

  def result
    policy.self_result
  end

  def driver?
    !!driver
  end

  def client?
    !!current_company&.client?
  end

  def owner?
    !!current_company&.owner?
  end

  def supplier?
    !!current_company&.supplier?
  end

  def constructor?
    !!current_company&.constructor?
  end

  def admin?
    !!current_user&.admin_or_dispatcher? || !!current_company&.admin?
  end

  def car_owner?
    owner? || constructor? || supplier?
  end

  def in_roles?(roles)
    return false unless current_company&.kind
    roles.include?(current_company.kind)
  end

  protected

  # TODO Check realization variants
  def self_validate
    true
  end

  # TODO Check realization variants
  def self_result
    []
  end

  def endpoint_class
    "#{self.class.name}::#{request_method}".classify.constantize
  end

  def endpoint_init
    endpoint_class.new(user: user, driver: driver, route_object: route_object, params: params).policy
  end
end
