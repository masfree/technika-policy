class Policy::EndpointBase < Policy::Base
  def policy
    @policy ||= endpoint_init
  end
end
