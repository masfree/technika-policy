class Policy::Factory
  attr_reader :user, :driver, :route_object, :route, :params

  def initialize(user:, driver:, route_object:, params:)
    @user = user
    @driver = driver
    @route_object = route_object
    @route = route_object.path.gsub('(.:format)', '')
    @params = params
  end

  def policy
    policy_class = case
                      when route.include?('/v1/drivers')
                        Policy::V1::Drivers
                      when route.include?('/v1/instances')
                        Policy::V1::Instances
                      when route.include?('/v1/orders')
                        Policy::V1::Orders
                      when route.include?('/v1/cabinet')
                        Policy::V1::Cabinet
                      when route.include?('/v1/tracker')
                        Policy::V1::Tracker
                      else
                        Policy::Base
                   end
    policy_class.new(user: user, driver: driver, route_object: route_object, params: params).policy
  end
end
