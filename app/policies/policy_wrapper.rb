class PolicyWrapper
  attr_reader :policy
  def initialize(user:, driver:, route_object:, params:)
    @policy = Policy::Factory.new(user: user, driver: driver, route_object: route_object, params: params).policy
  end

  def validate
    policy.validate
  end

  def result
    policy.result
  end

  def method_missing(method_name, *args, &blk)
    return policy.send(method_name, *args, &blk)
  end
end
